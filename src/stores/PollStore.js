import { writable } from 'svelte/store'

const PollStore = writable([
  {
    id: 1,
    question: 'Sein oder nicht sein?',
    answerA: 'oder',
    answerB: 'Shakespeare',
    votesA: 75,
    votesB: 3,
  },
]);

export default PollStore;